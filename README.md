﻿## Table of contents

**Clone the repository**

**Build the project using Gradle Wrapper tool**

**Connect to a AstraDB database from IntelliJ IDEA Database tool**

---

## Clone the repository

### Clone the repository using Git
Use these steps to clone the data-warehouses-starter repository using `git` tool. Cloning allows you to work on your files locally.

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Click Copy to clipboard button to copy the `git clone ...` command to clipboard
3. Create a directory on your local machine that will be the home of your future projects
4. Open a **Command Prompt**/**Terminal** window in the directory you just created and paste the clipboard content there<br>
`git clone git@bitbucket.org:danielpop/data-warehouses-starter.git`
5. Hit `ENTER` and there you go: a new folder containing a clone of this repository is created on your local machine. 
6. Change to that directory `cd data-warehouses-starter` and move to the next section (Build the project using Gradle Wrapper)

### Clone the repository from a ZIP archive
If you don't yet have Git, and don't want/need/like it, just download the repository as a ZIP archive 
(click Downloads on the left panel and then Download repository) and unpack it locally. Move to the next section.

## Build the project using Gradle Wrapper

**Prerequisite: Java 21 installed and `JAVA_HOME` environment variable set to point to Java 21 installation.**


Run `gradlew build` to build the project. This will
 download Gradle (if not already on your system) and will build the project.
If you want to build a self-contains (fat) JAR then run `gradlew build shadowJar`

---

## Connect to a AstraDB database from IntelliJ IDEA Database tool

In order to use IntelliJ IDEA's Database tool to connect, visualize and manage data in a DataStax AstraDB database, do the following:

1. Download the Secure Connect Bundle (the file `secure-connect-<db name>.zip`) for your database; this file bundles the required SSL/TLS certificates
2. Unpack it to a folder on your local machine (referred to as `<cert path>` below)
3. In the Database tool window, click Add button and then select Data Source > Apache Cassandra
4. Configure the SSL/TLS certificates in the SSH/SSL tab

* CA file: select `<cert path>/ca.crt` file
* Client certificate file: select `<cert path>/cert.` file
* Client key file: select `<cert path>/key.` file

6. Configure the database credentials in the General tab

* Host: `<db id>-<db-region>.db.astra.datastax.com` (e.g., `5dc70547-1ec8-4195-bfe2-000000000000-europe-west1`)
* Port: `29042`
* Authentication: User & Password
* User: `<clientId from <db name>-token.json file>`
* Password: `<secret from <db name>-token.json file>`

6. Test connection and Save