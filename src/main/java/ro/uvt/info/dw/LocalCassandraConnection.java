package ro.uvt.info.dw;

import com.datastax.oss.driver.api.core.CqlSession;

import java.io.Closeable;
import java.net.InetSocketAddress;

import static java.lang.System.out;

public class LocalCassandraConnection implements Closeable {
    private CqlSession session;

    public LocalCassandraConnection connect(String node, Integer port, String user, String password) {
        var sessionBuilder = CqlSession
            .builder()
            .addContactPoint(new InetSocketAddress(node, port));

        if (user!=null && password!=null) {
            sessionBuilder.withAuthCredentials(user, password);
        }

        session = sessionBuilder.build();
        printMetadata();
        return this;
    }

    private void printMetadata() {
        final var metadata = session.getMetadata();
        out.printf("Connected to cluster: %s\n", metadata.getClusterName());
        for (final var node : metadata.getNodes().values()) {
            out.printf(
                "Datacenter: %s; Host: %s; Rack: %s\n",
                node.getDatacenter(),
                node.getHostId(),
                node.getRack());
        }
    }

    public CqlSession session() {
        return this.session;
    }

    public LocalCassandraConnection createKeyspace(
        String keyspaceName,
        String replicationStrategy,
        int replicationFactor) {

        var cqlCmd = "CREATE KEYSPACE IF NOT EXISTS " +
            keyspaceName + " WITH replication = {" +
            "'class':'" + replicationStrategy +
            "','replication_factor':" + replicationFactor +
            "};";

        session.execute(cqlCmd);
        return this;
    }

    public LocalCassandraConnection dropKeyspace(String keyspaceName) {
        session.execute("DROP KEYSPACE IF EXISTS " + keyspaceName);
        return this;
    }

    public void close() {
        session.close();
    }
}
