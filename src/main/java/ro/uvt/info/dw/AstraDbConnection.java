package ro.uvt.info.dw;

import com.datastax.oss.driver.api.core.CqlSession;

import java.nio.file.Paths;

public class AstraDbConnection {

    // This source file is obtained from the AstraDB database's Connect page
    // https://astra.datastax.com/org/<your-organisation>/database/<your-database>/connect
    // CLIENT_ID and CLIENT_SECRET can be obtained from there too
    private static final String CLIENT_ID = "<CLIENT_ID>";
    private static final String CLIENT_SECRET = "<CLIENT_SECRET>";

    public static void main(String[] args) {
        // Create the CqlSession object:
        try (CqlSession session = CqlSession.builder()
                // TODO - CloudSecureConnectBundle filename (secure-connect-dw-class-2025.zip) needs to be adjusted to match your database name
                .withCloudSecureConnectBundle(Paths.get("src\\main\\resources\\secure-connect-dw-class-2025.zip"))
                .withAuthCredentials(CLIENT_ID, CLIENT_SECRET)
                .build()) {
            // Select the release_version from the system.local table:
            var rs = session.execute("select release_version from system.local");
            var row = rs.one();
            //Print the results of the CQL query to the console:
            if (row != null) {
                System.out.printf("Release: %s\n", row.getString("release_version"));
            } else {
                System.err.println("An error occurred.");
            }
        }
        System.exit(0);
    }
}