package ro.uvt.info.dw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.cassandra.CqlSessionBuilderCustomizer;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import ro.uvt.info.dw.springastradb.AuthorsRepository;
import ro.uvt.info.dw.springastradb.DataStaxAstraProperties;

import java.nio.file.Path;

// Main application class with main method that runs the Spring Boot app
@SpringBootApplication
@EnableConfigurationProperties(DataStaxAstraProperties.class)
public class AstraDbApplication {

    // Connecting the Spring Boot application to an Astra DB is detailed below
    // https://medium.com/building-the-open-data-stack/build-a-goodreads-clone-with-spring-boot-and-astra-db-part-4-ed2dc22537e
    // in the 'Connecting the Spring Boot application to Astra DB' section
    public static void main(String[] args) {
        // Keep only this line for a running Spring Application
        //SpringApplication.run(AstraDbApplication.class, args);

        // The lines up to the end of the function are only for demo purposes to verify
        // that database connection works properly and they should be removed after
        // setup is completed successfully.
        SpringApplication
            .run(AstraDbApplication.class, args)
            .getBean(AuthorsRepository.class)
            .findAll()
            .forEach(System.out::println);
        System.exit(0);
    }

    // This is necessary to have the Spring Boot app use the Astra secure bundle
    // to connect to the database
    @Bean
    public CqlSessionBuilderCustomizer sessionBuilderCustomizer(DataStaxAstraProperties astraProperties) {
        Path bundle = astraProperties.getSecureConnectBundle().toPath();
        return builder -> builder.withCloudSecureConnectBundle(bundle);
    }
}