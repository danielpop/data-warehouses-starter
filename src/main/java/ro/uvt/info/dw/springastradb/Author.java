package ro.uvt.info.dw.springastradb;

import org.springframework.data.annotation.Id;
import org.springframework.data.cassandra.core.mapping.Table;

import java.time.LocalDate;

@Table("authors")
public record Author (
    @Id String id,
    String first_name,
    String last_name,
    String url,
    LocalDate join_date) {
}
