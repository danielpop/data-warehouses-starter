package ro.uvt.info.dw.springastradb;

import org.springframework.data.cassandra.repository.CassandraRepository;

public interface AuthorsRepository extends CassandraRepository<Author, String> {
}
