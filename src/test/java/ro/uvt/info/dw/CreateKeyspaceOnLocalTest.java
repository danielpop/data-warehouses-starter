package ro.uvt.info.dw;

import org.junit.Ignore;
import org.junit.Test;

public class CreateKeyspaceOnLocalTest {

    public static String CLUSTER_IP  = "localhost";
    private static int PORT = 9042;
    private static String USER = "cassandra";
    private static String PASSWORD = "cassandra";

    @Test
    @Ignore // TODO: Uncomment this line if you have a local Cassandra cluster
    public void testCreateKeyspaceSuccessful() {
        var keyspaceName = "test1";
        var client = new LocalCassandraConnection();

        var result = client
            .connect(CLUSTER_IP, PORT, USER, PASSWORD)
            .createKeyspace(keyspaceName, "SimpleStrategy", 3)
            .session()
            .execute("SELECT * FROM system_schema.keyspaces;");

        var matchedKeyspaces = result.all().stream()
            .filter(r -> keyspaceName.toLowerCase().equals(r.getString(0)))
            .map(r -> r.getString(0))
            .toList();

        client
            .dropKeyspace(keyspaceName)
            .close();

        assert matchedKeyspaces.size() == 1;
        assert matchedKeyspaces.get(0).equals(keyspaceName.toLowerCase());
    }
}
